//
//  Generated file. Do not edit.
//

// clang-format off

#include "generated_plugin_registrant.h"

#include <mavlink_plugin/mavlink_plugin.h>

void fl_register_plugins(FlPluginRegistry* registry) {
  g_autoptr(FlPluginRegistrar) mavlink_plugin_registrar =
      fl_plugin_registry_get_registrar_for_plugin(registry, "MavlinkPlugin");
  mavlink_plugin_register_with_registrar(mavlink_plugin_registrar);
}
