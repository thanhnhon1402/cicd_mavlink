// ignore_for_file: avoid_print, non_constant_identifier_names
import 'dart:ffi'; //Interface for interoperability with the C.
import 'package:ffi/ffi.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mavlink_plugin/mavlink_wrapper.dart';
import 'package:mavlink_plugin/mavlink_wrapper_type.dart';

bool compare_list(var result, var heartBeat) {
  if (result[0] == heartBeat.custom_mode &&
      result[1] == heartBeat.type &&
      result[2] == heartBeat.autopilot &&
      result[3] == heartBeat.base_mode &&
      result[4] == heartBeat.system_status &&
      result[5] == heartBeat.mavlink_version) {
    return true;
  } else {
    return false;
  }
}

void main() {
  print("Bat dau chuong trinh test");
  group('Mavlink wapper return with function sizeof(), ', () {
    test('getSizeMavlinkMessage error', () {
      var SizeMess = MavlinkWrapper().getSizeMavlinkMessage();
      expect(SizeMess > 250 && SizeMess < 320, true); //SizeMess = 291
    });
    test('getSizeMavlinkStatus error', () {
      var SizeStatus = MavlinkWrapper().getSizeMavlinkStatus();
      expect(SizeStatus > 30 && SizeStatus < 45, true); //SizeStatus = 45
    });
    test('getSizeHeartBeat error', () {
      var SizeHearBeat = MavlinkWrapper().getSizeHeartBeat();
      expect(SizeHearBeat > 8 && SizeHearBeat < 15, true);//SizeHearBeat = 9
      // Theo doi dau ra
    });
    test('mavlinkMsgToSendBuffer', () {
      var sendBuffer = calloc<Uint8>(100);
      var msg = calloc<MavlinkMessage>(
          MavlinkWrapper().getSizeMavlinkMessage()); //receiveMavlinkMessage
      var sendBufferLen =
          MavlinkWrapper().mavlinkMsgToSendBuffer(sendBuffer, msg);
      expect(sendBufferLen == 12|| sendBufferLen == 25, true); // MAVLink 2 Packet Format, khong xay ra voi code sendBufferLen == 25 (signature_len)
    });
    test('mavlinkMsgHeartbeatEncodeChan', () {
      
      var msg = calloc<MavlinkMessage>(
          MavlinkWrapper().getSizeMavlinkMessage()); //receiveMavlinkMessage
      var heartbeat = calloc<MavlinkHeartbeat>(
           MavlinkWrapper().getSizeHeartBeat()); //heartbeat
          var EncodeChan = MavlinkWrapper().mavlinkMsgHeartbeatEncodeChan(0, 0, 0, msg, heartbeat);
      expect(EncodeChan, 21);
    });
  });
  group('mavlinkParseChar, ', () {
    test('mavlinkParseChar error', () {
      var heartBeat_result = [0, 2, 3, 81, 3, 3]; // Gia tri chay thu
      var event = [
        [253, 9, 0, 0, 50, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2, 3, 81, 3, 3, 90, 108],
        [253, 9, 0, 0, 63, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2, 3, 81, 3, 3, 236, 154],
        // [253, 25, 0, 0, 77, 1, 1, 22, 0, 0, 0, 133, 156, 71, 173, 4, 255, 255, 83, 84, 65, 84, 95, 82, 85, 78, 84, 73, 77, 69, 0, 0, 0, 0, 6, 10, 3]
      ];
      var receiveMavlinkStatus =
          calloc<MavlinkStatus>(MavlinkWrapper().getSizeMavlinkMessage());
      var receiveMavlinkMessage =
          calloc<MavlinkMessage>(MavlinkWrapper().getSizeMavlinkStatus());

      for (final parse in event) {
        // print(parse);
        for (final c in parse) {
          var parseStatus = MavlinkWrapper().mavlinkParseChar(
              0, c, receiveMavlinkMessage, receiveMavlinkStatus);
          var receiveMavlinkMessageRef = receiveMavlinkMessage.ref;
          if (parseStatus == 1) {
            expect(c, parse[parse.length - 1]);
            if (receiveMavlinkMessageRef.msgid == 0) {
              var heartBeat =
                  calloc<MavlinkHeartbeat>(MavlinkWrapper().getSizeHeartBeat());
              MavlinkWrapper()
                  .mavlinkMsgHeartbeatDecode(receiveMavlinkMessage, heartBeat);
              var heartBeatRef = heartBeat.ref;
              expect(compare_list(heartBeat_result, heartBeatRef), true);
            }
          }
        }
      }
    });
  });
}


// typedef struct __mavlink_heartbeat_t {
//  uint32_t custom_mode; /*<  A bitfield for use for autopilot-specific flags*/
//  uint8_t type; /*<  Vehicle or component type. For a flight controller component the vehicle type (quadrotor, helicopter, etc.). For other components the component type (e.g. camera, gimbal, etc.). This should be used in preference to component id for identifying the component type.*/
//  uint8_t autopilot; /*<  Autopilot type / class. Use MAV_AUTOPILOT_INVALID for components that are not flight controllers.*/
//  uint8_t base_mode; /*<  System mode bitmap.*/
//  uint8_t system_status; /*<  System status flag.*/
//  uint8_t mavlink_version; /*<  MAVLink version, not writable by user, gets added by protocol because of magic data type: uint8_t_mavlink_version*/
// } mavlink_heartbeat_t;
