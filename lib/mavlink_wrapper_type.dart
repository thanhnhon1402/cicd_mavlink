// ignore_for_file: non_constant_identifier_names, unused_import, avoid_return_types_on_setters

import 'dart:ffi'; // For FFI
import 'dart:typed_data';

import 'package:ffi/ffi.dart';

class MavlinkMessage extends Struct {
  @Uint16()
  external int checksum;
  @Uint8()
  external int magic;
  @Uint8()
  external int len;
  @Uint8()
  external int incompat_flags;
  @Uint8()
  external int compat_flags;
  @Uint8()
  external int seq;
  @Uint8()
  external int sysid;
  @Uint8()
  external int compid;

  @Uint8()
  external int bits;
  int get msgid => getBits(0, 24);
  void set bold(int value) => setBits(0, 3, value);
  int getBits(int offset, int length) => bits.getBits(offset, length);
  void setBits(int offset, int length, int value) {
    bits = bits.setBits(offset, length, value);
  }

  external Pointer<Uint64> payload64;
  external Pointer<Uint8> ck;
  external Pointer<Uint8> signature;
}

/// Extension to use a 64-bit integer as bit field.
extension IntBitField on int {
  static int _bitMask(int offset, int lenght) => ((1 << lenght) - 1) << offset;

  /// Read `length` bits at `offset`.
  ///
  /// Truncates everything.
  int getBits(int offset, int length) {
    final mask = _bitMask(offset, length);
    return (this & mask) >> offset;
  }

  /// Returns a new integer value in which `length` bits are overwritten at
  /// `offset`.
  ///
  /// Truncates everything.
  int setBits(int offset, int length, int value) {
    final mask = _bitMask(offset, length);
    return (this & ~mask) | ((value << offset) & mask);
  }
}

class MavlinkStatus extends Struct {
  @Uint8()
  external int msg_received;
  @Uint8()
  external int buffer_overrun;
  @Uint8()
  external int parse_error;

  @Uint32()
  external int parse_state;
  @Uint8()
  external int packet_idx;
  @Uint8()
  external int current_rx_seq;
  @Uint8()
  external int current_tx_seq;
  @Uint16()
  external int packet_rx_success_count;
  @Uint16()
  external int packet_rx_drop_count;
  @Uint8()
  external int flags;
  @Uint8()
  external int signature_wait;
  external Pointer<Struct> signing;
  external Pointer<Struct> signing_streams;
}

class MavlinkHeartbeat extends Struct {
  @Uint32()
  external int custom_mode;
  @Uint8()
  external int type;
  @Uint8()
  external int autopilot;
  @Uint8()
  external int base_mode;
  @Uint8()
  external int system_status;
  @Uint8()
  external int mavlink_version;
}
