#import "MavlinkPlugin.h"
#if __has_include(<mavlink_plugin/mavlink_plugin-Swift.h>)
#import <mavlink_plugin/mavlink_plugin-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "mavlink_plugin-Swift.h"
#endif

@implementation MavlinkPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftMavlinkPlugin registerWithRegistrar:registrar];
}
@end
