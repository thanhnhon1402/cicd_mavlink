// ignore_for_file: unnecessary_brace_in_string_interps, unused_local_variable, avoid_print, non_constant_identifier_names

import 'dart:async';

import 'package:flutter/services.dart';

import 'dart:ffi';
import 'dart:typed_data';
import 'package:ffi/ffi.dart';

import 'dart:io';

import 'mavlink_wrapper.dart';
import 'mavlink_wrapper_type.dart';

class MavlinkConnection {
  static var address = '0.0.0.0';
  static var port = 5760;

  var connection = Socket.connect(address, port);
  //Socket ket noi cuc bo voi bo dieu khien tu xa Uint8List
  late Socket socket;

  var mavlinkWrapper = MavlinkWrapper();

  MavlinkConnection() {
    connect();
  }
//TH1: mavlinkWrapper.getSizeMavlinkMessage()

  void connect() async {
    socket = await connection;
    print('[MavlinkConnection]: Connected to ${address}:${port}');

    var receiveMavlinkMessage =
        calloc<MavlinkMessage>(mavlinkWrapper.getSizeMavlinkMessage());
    print('object');
    print(mavlinkWrapper.getSizeMavlinkMessage().toString());
    //calloc khoi tao 1 bo nho co kich thuoc giong nhu mavlink và tra dia chi address
    var receiveMavlinkStatus =
        calloc<MavlinkStatus>(mavlinkWrapper.getSizeMavlinkStatus());
    //address=0x7fd540246fa0

    socket.listen((Uint8List event) {
      print(
          "_________________________________________________________________________");
      //Tra ve 1 chuoi cac phan tu cua luong
      print('Event return:' + event.toString());
      for (final c in event) {
        //Hàm sẽ phân tích cú pháp từng byte một và trả về gói tin hoàn chỉnh một lần,
        // nó có thể được giải mã thành công. Hàm này sẽ trả về 0 hoặc 1
        var parseStatus = mavlinkWrapper.mavlinkParseChar(
            0, //ID của kênh được phân tích cú pháp
            c, //c: The char to parse
            receiveMavlinkMessage,
            receiveMavlinkStatus); //Ham tra ve gia tri 0 voi 1
        // print(c);
        if (parseStatus == 1) {
          print('Gia tri cua c: $c');
          print(receiveMavlinkMessage);
          print(receiveMavlinkStatus);
          var receiveMavlinkMessageRef =
              receiveMavlinkMessage.ref; //Tao tham chieu truy cap cua bo nho
          // print(receiveMavlinkMessageRef);
          // print(receiveMavlinkMessage);
          // Hoi anh PHONG
          //su dung get bit
          print('getbit: ' + receiveMavlinkMessageRef.msgid.toString());
          if (receiveMavlinkMessageRef.msgid == 0) {
            var receiveMavlinkStatusRef =
                receiveMavlinkStatus.ref; //Tao tham chieu truy cap cua bo nho

            var heartBeat = calloc<MavlinkHeartbeat>(
                mavlinkWrapper.getSizeHeartBeat()); //mavlink_msg_hearbeat.h
            //mavlinkMsgHeartbeatDecode
            var a = mavlinkWrapper.mavlinkMsgHeartbeatDecode(
                receiveMavlinkMessage, heartBeat);
            print('Gia tri mavlinkMsgHeartbeatDecode: $a');
            var heartBeatRef = heartBeat.ref;
            // print(receiveMavlinkMessage);
            // print(receiveMavlinkStatus);
            print('[MavlinkConnection]: heartBeat: ' +
                heartBeatRef.custom_mode
                    .toString() + // A bitfield for use for autopilot-specific flags*/
                ', ' +
                heartBeatRef.type
                    .toString() + // Vehicle or component type. For a flight controller component the vehicle type (quadrotor, helicopter, etc.). For other components the component type
                // (e.g. camera, gimbal, etc.). This should be used in preference to component id for identifying the component type.*/
                ', ' +
                heartBeatRef.autopilot
                    .toString() + /*<  Autopilot type / class. Use MAV_AUTOPILOT_INVALID for components that are not flight controllers.*/
                ', ' +
                heartBeatRef.base_mode.toString() + /*<  System mode bitmap.*/
                ', ' +
                heartBeatRef.system_status
                    .toString() + /*<  System status flag.*/
                ', ' +
                heartBeatRef.mavlink_version
                    .toString()); /*<  MAVLink version, not writable by user, gets added by protocol because of magic data type: uint8_t_mavlink_version*/

            calloc.free(heartBeat);
          }

          calloc.free(receiveMavlinkMessage); //Giai phong vung nho
          calloc.free(receiveMavlinkStatus);

          receiveMavlinkMessage =
              calloc<MavlinkMessage>(mavlinkWrapper.getSizeMavlinkMessage());
          receiveMavlinkStatus =
              calloc<MavlinkStatus>(mavlinkWrapper.getSizeMavlinkStatus());
        }
      }
    });
    print('Bat dau dong for');
    socket.handleError((Object error) {
      print('[MavlinkConnection]: Connection Error: $error');
      calloc.free(receiveMavlinkMessage);
      calloc.free(receiveMavlinkStatus);
      socket.close();
    });
    //mavlinkMsgHeartbeatToSendBuffer
    for (int i = 0; i < 100; i++) {
      socket.add(mavlinkMsgHeartbeatToSendBuffer(0, 0, 0, 0, 0, 0, 0, 0));

      socket.flush();
      // print('mavlinkMsgHeartbeatToSendBuffer' +
      //     mavlinkMsgHeartbeatToSendBuffer(0, 0, 0, 0, 0, 0, 0, 0).toString());
      await Future.delayed(const Duration(seconds: 1));
    }
  }

  List<int> mavlinkMsgHeartbeatToSendBuffer(
      int system_id,
      int component_id,
      int chan,
      int type,
      int autopilot,
      int base_mode,
      int custom_mode,
      int system_status) {
    var sendBuffer = calloc<Uint8>(100);
    var msg = calloc<MavlinkMessage>(
        mavlinkWrapper.getSizeMavlinkMessage()); //receiveMavlinkMessage
    var heartbeat =
        calloc<MavlinkHeartbeat>(mavlinkWrapper.getSizeHeartBeat()); //heartbeat

    mavlinkWrapper.mavlinkMsgHeartbeatEncodeChan(
        system_id, component_id, chan, msg, heartbeat);
    // print('EcodeChan: $EcodeChan');
    var sendBufferLen = mavlinkWrapper.mavlinkMsgToSendBuffer(sendBuffer, msg);

    print('sendBufferLen: $sendBufferLen');
    var sendBufferList = sendBuffer.asTypedList(sendBufferLen);
    print("sendBufferList; $sendBufferList");
    var sendBufferListRet = [...sendBufferList];
    print('sendBufferListRet: $sendBufferListRet');
    calloc.free(sendBuffer);
    calloc.free(msg);
    calloc.free(heartbeat);

    return sendBufferListRet;
  }
}

class MavlinkPlugin {
  static const MethodChannel _channel = MethodChannel('mavlink_plugin');

  static Future<String?> get platformVersion async {
    var mavlinkConnection = MavlinkConnection(); // Connect Mavlink

    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
// typedef struct __mavlink_heartbeat_t {
//  uint32_t custom_mode; /*<  A bitfield for use for autopilot-specific flags*/
//  uint8_t type; /*<  Vehicle or component type. For a flight controller component the vehicle type (quadrotor, helicopter, etc.). For other components the component type (e.g. camera, gimbal, etc.). This should be used in preference to component id for identifying the component type.*/
//  uint8_t autopilot; /*<  Autopilot type / class. Use MAV_AUTOPILOT_INVALID for components that are not flight controllers.*/
//  uint8_t base_mode; /*<  System mode bitmap.*/
//  uint8_t system_status; /*<  System status flag.*/
//  uint8_t mavlink_version; /*<  MAVLink version, not writable by user, gets added by protocol because of magic data type: uint8_t_mavlink_version*/
// } mavlink_heartbeat_t;
